Research Toolkits is a self-service portal for researchers to get access to and manage their research computing services. We continue to expand self-service options. Currently faculty and their designees can manage access to project-based virtual machines with storage and to the Duke Compute Cluster (DCC). With Research Toolkits:

-   Researchers can see if they have access to projects with virtual machines, groups in the DCC, or lookup the point of contact(POC) for your lab or group
-   Duke faculty can create one or more projects, allocate compute resources across their projects, and can grant access to students or designees per project. Faculty automatically receive a free allocation of 4 CPU cores, 40GB of RAM, and 200GB of Storage
-   Duke Compute Cluster POCs can add and remove users to their lab or group. If you are a Duke faculty member and are interested in setting up a new group to purchase equipment for the cluster or to try out the cluster using common access, [contact(link is external)](https://rc.duke.edu/contact/ "https://rc.duke.edu/contact/") [us(link is external)](https://rc.duke.edu/contact/ "https://rc.duke.edu/contact/")

## How to get help

## Related Help Articles