## Logging into the DCC

As a shared computing resource, all work done on the DCC must be done by submitting jobs to the [scheduler](https://oit-rc.pages.oit.duke.edu/rcsupportdocs/dcc/slurm/). Login nodes must not be used to execute computing tasks.

Acceptable use of login nodes include:

-   lightweight file transfers,
-   script and configuration file editing,
-   job submission and monitoring,
-   [BaseSpace BaseMount](https://basemount.basespace.illumina.com/) - users can use basemount to mount BaseSpace into their home directories.

To minimize disruption and ensure a comfortable working environment for users, resource limits are enforced on login nodes, and processes started there will automatically be terminated if their resource usage (including CPU time, memory and run time) exceed those limits.

If you need to open a GUI interface on the DCC (such as RStudio or Jupyter), use the [Open OnDemand service.](https://oit-rc.pages.oit.duke.edu/rcsupportdocs/OpenOnDemand/)

## Logging into the DCC

Login to the DCC using `ssh netid@dcc-login.oit.duke.edu`, (note: VPN is not required, but MFA **is required**).

```
kk338@CDSS-5630 ~ % ssh kk338@dcc-login.oit.duke.edu
Password: 
Duo two-factor login for kk338

Enter a passcode or select one of the following options:

1. Duo Push to XXX-XXX-4007
2. Phone call to XXX-XXX-4007
3. Phone call to XXX-XXX-9784
4. SMS passcodes to XXX-XXX-4007 (next code starts with: 2)

Passcode or option (1-4): 1
Success. Logging you in...
Last login: Tue Dec 21 11:07:41 2021 from xxx
################################################################################
# MOTD                                                                         # 
# My patch window is wednesday 03:00                                           # ################################################################################
kk338@dcc-login-03  ~ $
```

### A word on ssh clients

Linux and MacOS systems generally come with a SSH client already installed and you can `ssh` directly from a terminal window. For Windows users you will need an ssh client such as [PuTTY](https://putty.org/). Advanced users may consider installing WSL (Windows Subsystem for Linux), see [installation instructions here](https://docs.microsoft.com/en-us/windows/wsl/install), but beware DCC support staff do not offer support for your local computer and software installations. Other ssh clients may work or not, but beware of clients that create persistent sessions to the DCC.

## SSH Keys

Setting up ssh keys from your workstation will greatly simplify the login and file transfer process by creating a secure key based session between your workstation and the DCC instead of using your password with MFA.

To generate a key pair on a Macintosh or Linux machine:

`ssh-keygen -t rsa -b 4096`

The public key generated will be stored in the file:

`~/.ssh/id_rsa.pub`

To enable ssh public key authentication update your “SSH Public keys” under “Advanced User Options” at: [idms-web.oit.duke.edu/portal](https://idms-web.oit.duke.edu/portal).

View more general information about ssh public key authentication at [ssh.com](https://www.ssh.com/ssh/public-key-authentication#key-pair---public-and-private).

```
kk338@CDSS-5630 ~ % ssh kk338@dcc-login.oit.duke.edu
Enter passphrase for key '/Users/kk338/.ssh/id_rsa': 
 Last login: Tue Dec 21 11:13:41 2021 from xxx
################################################################################
# MOTD                                                                         # 
# My patch window is wednesday 03:00                                           # ################################################################################
kk338@dcc-login-03  ~ $
```